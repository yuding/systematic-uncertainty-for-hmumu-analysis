from ROOT import *
import argparse

def SetConfig():
    parser = argparse.ArgumentParser(description="Set configuration")
    parser.add_argument("-d","--DISD", default=601506, help="Input sample DISD number.")
    parser.add_argument("-v","--version",default= "mc23a", help="Input sample version.")
    return parser

def ReadTxTFile(InputTxtFile):
    SystmaticList = []
    SystmaticNameList = []
    SystmaticDir = {}
    with open(InputTxtFile, 'r') as file:
        for line in file:
            SystmaticList.append(line.strip())
    for i in range(len(SystmaticList)):
        SystmaticNameList.append(SystmaticList[i].split('__')[0])
    for i in range(len(SystmaticNameList)):
        if SystmaticNameList[i] not in SystmaticDir:
            SystmaticDir[SystmaticNameList[i]] = []
            SystmaticDir[SystmaticNameList[i]].append(SystmaticList[i])
        else:
            SystmaticDir[SystmaticNameList[i]].append(SystmaticList[i])
    return SystmaticDir

def ReadTree(SystmaticDir, InputRootFile):
    file1 = TFile.Open(InputRootFile)
    tree = file1.Get("tree_Hmumu")
    Nominal_Signal_Yield = 0
    Weight_syst_Dir = {}
    Syst_signal_yield_Dir = {}
    for name in SystmaticDir:
        Weight_syst_Dir[name] = []
        Syst_signal_yield_Dir[name] = []
    for name,values in SystmaticDir.items():
        for i in range(len(values)):
            Weight_syst_Dir[name].append(0)
            Syst_signal_yield_Dir[name].append(0)
    for entry in range(tree.GetEntries()):
    #for entry in range(5000):
        tree.GetEntry(entry)
        if (entry % 50000) == 0:
            print("Readout: ",entry,int(tree.GetEntries()))
        nominal_weight = tree.weight
        Reco_dilep_mass = tree.Muons_Minv_MuMu_Fsr
        if 110 < Reco_dilep_mass < 160:
            Nominal_Signal_Yield += nominal_weight
        for name,values in SystmaticDir.items():
            for i in range(len(values)):
                W_syst = getattr(tree,values[i])
                Weight_syst_Dir[name][i] = W_syst * nominal_weight
                if 110 < Reco_dilep_mass < 160:
                    Syst_signal_yield_Dir[name][i] += Weight_syst_Dir[name][i]
    print("SystematicName: ",SystmaticDir)
    print("Nonimal_Yield: ", Nominal_Signal_Yield)
    print("Yield: ", Syst_signal_yield_Dir)
    return  Nominal_Signal_Yield, Syst_signal_yield_Dir

def UncertRatioCal(Nominal_Signal_Yield, Syst_signal_yield_list):
    Syst_ratio_list = []
    for i in range(len(Syst_signal_yield_list)):
        uncert_ratio = (Syst_signal_yield_list[i] - Nominal_Signal_Yield) / Nominal_Signal_Yield
        Syst_ratio_list.append()      

if __name__ == '__main__':
  
  # Set the configuraion
    parser = SetConfig()
    args = parser.parse_args()

    InputTxtFile = "Syst_Name_eff.txt"
    InputRootFileName = ''
    if int(args.DISD) == 601506 and args.version == 'mc23a':
        InputRootFileName = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/Run3_common_ntuples/Prod_v1/Systematics/mc23a/mc23_13p6TeV.601506.PhPy8EG_PDF4LHC21_MiNLO_ggH125_mumu_HmumuSR.root'
    else:
        print("======= Not find the input RootFile! ========")
    SystmaticDir = ReadTxTFile(InputTxtFile)
    Nominal_Signal_Yield, Syst_signal_yield_Dir = ReadTree(SystmaticDir, InputRootFileName)
    

