from ROOT import *
import argparse

def SetConfig():
    parser = argparse.ArgumentParser(description="Set configuration")
    parser.add_argument("-d","--DISD", default=601506, help="Input sample DISD number.")
    parser.add_argument("-v","--version",default= "mc23a", help="Input sample version.")
    return parser

def ReadTxTFile(InputTxtFile):
    SystmaticList = []
    SystmaticNameList = []
    SystmaticDir = {}
    with open(InputTxtFile, 'r') as file:
        for line in file:
            SystmaticList.append(line.strip())
    for i in range(len(SystmaticList)):
        SystmaticNameList.append(SystmaticList[i].split('__')[0])
    for i in range(len(SystmaticNameList)):
        if SystmaticNameList[i] not in SystmaticDir:
            SystmaticDir[SystmaticNameList[i]] = []
            SystmaticDir[SystmaticNameList[i]].append(SystmaticList[i])
        else:
            SystmaticDir[SystmaticNameList[i]].append(SystmaticList[i])
    return SystmaticDir

def ReadTree(SystmaticDir,args):
    Syst_signal_yield_Dir = {}
    for name in SystmaticDir:
        Syst_signal_yield_Dir[name] = []
    for name,values in SystmaticDir.items():
        for i in range(len(values)):
            Syst_signal_yield_Dir[name].append(0)
    if int(args.DISD) == 601506 and args.version == 'mc23a':
        for name,values in SystmaticDir.items():
            for i in range(len(values)):
                InputRootFileName = ''
                print("InputRootFile, Systematic : ",SystmaticDir[name][i])
                InputRootFileName = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/Run3_common_ntuples/Prod_v1/Systematics/mc23a/Systematics/{}/mc23_13p6TeV.601506.PhPy8EG_PDF4LHC21_MiNLO_ggH125_mumu_HmumuSR.root'.format(SystmaticDir[name][i])
                file1 = TFile.Open(InputRootFileName)
                tree = file1.Get("tree_Hmumu")
                for entry in range(tree.GetEntries()):
                    tree.GetEntry(entry)
                    #if (entry % 100000) == 0:
                        #print("Readout: ",entry,int(tree.GetEntries()))
                    W_syst = tree.weight
                    Reco_dilep_mass = tree.Muons_Minv_MuMu_Fsr
                    if 110 < Reco_dilep_mass < 160:
                        Syst_signal_yield_Dir[name][i] += W_syst
                print("Syst_signal_yield: ",Syst_signal_yield_Dir[name][i])

    else:
        print("======= Not find the input RootFile! ========")
    return  Syst_signal_yield_Dir    

if __name__ == '__main__':
  
  # Set the configuraion
    parser = SetConfig()
    args = parser.parse_args()

    InputTxtFile = "Syst_Name_Reso.txt"
    SystmaticDir = ReadTxTFile(InputTxtFile)
    Syst_signal_yield_Dir = ReadTree(SystmaticDir, args)
    

